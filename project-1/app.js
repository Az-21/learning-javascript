//--------------------------------//
//Comments
//--------------------------------//

//Single-line comment
/* Multiple line Comment */


//--------------------------------//
//Console log
//--------------------------------//

console.log("This message will be displayed on browser's console"); //String
console.log("123");
console.log(222);                                               //Numbers
console.log(true);                                                  //Bool
console.log([1,2,3,4]);                                             //Array
console.log({a:1, b:2});                                            //Object
console.table({a:1, b:2});                                          //Table
console.error("Insert the error message here");                     //Errors
console.warn("Insert the warning message");                         //Warning

//console.clear();                                                    //Clear



//--------------------------------//
//Declaring Variables
//--------------------------------//

//Var Method
var name_1 = "Abhishek Choudhary";        //Initilization is required
console.log(name_1);
name_1 = "Akshat";
console.log(name_1);

//Let Method
let name_2 = "Abhishek";                  //let auto selects
console.log(name_2);
name_2 = "Akshat Choudhary";
console.log(name_2);

//Const Method
const name_3 = "Constant name";        //Initilization is required
console.log(name_3);      /* commented to prevent errors
name_3 = "Akshat";
console.log(name_3);      */

//Object Method
let person_1_data = {
  name_data: "Akshat",
  age_data: 10,
  haircolor_data: "Black",
}
console.log(person_1_data.name_data);
console.log(person_1_data.age_data);
console.log(person_1_data.haircolor_data);


//Finding the type of var/let/const
console.log(typeof person_1_data.name_data);
console.log(typeof person_1_data.age_data);
console.log(typeof person_1_data.haircolor_data);
console.log(typeof person_1_data);


//Null
let value_of_x = null;
console.log(value_of_x);


//console.clear();
