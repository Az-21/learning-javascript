//For Loop

let rgba = ["Red", "Green", "Blue", "Transparancy"];

//using for Loop
console.log("Using for loop");
for(let i =0; i <= rgba.length-1; i = i +1){
  console.log(rgba[i]);
}


//Using while loop
console.log("\nUsing while loop");
let index = 0;
while(index < rgba.length){
  console.log(rgba[index]);
  index = index + 1;
}

let bHeight = window.outerHeight;
let bWidth = window.outerWidth;

console.log(bHeight);
console.log(bWidth);
