let today = new Date();
console.log(today);
console.log(today.getDate());
console.log(today.getTime());

//if
let a = 2;
let b = '2';

if(a === b){
  console.log("a and b are identical");
}
else if(a!==b){
  console.log("a and b are equal but possess different data type");
}
else{
  console.log("Error");
}


//function
function my_first_function(){
  console.log("My first function was called");
  return "Hello";
}

let val = my_first_function();
console.log(val);

function add_invert(num1, num2){
  return ((-1)*(num1 + num2))
}

console.log(add_invert(1,3));
