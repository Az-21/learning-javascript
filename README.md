# Learning JavaScript
This is a personal repo I use to keep notes and a latex reference guide.

### [JavaScript Reference Guide][1]

### Topics Covered
1. Comments
  * Single line
  * Multiline
2. Print
  * String
  * Number
  * Bool
  * Array
  * Table
  * Warning
  * Error
3. Declaring variables

4. Declaring objects

5. Printing variables

6. Link JS to HTML

7. Find datatype of a variable

8. Data type conversion
  * String to other
  * Number to other
9. Math manipulation
  * Constants
  * Round and controlled round
  * Root
  * Absolute
  * Power
  * Random number generator
10. String manipulation
  * Concatenation
  * Escape sequence
  * Changing case
  * Character in string
  * Trimming/splicing an array
  * Split (extract elements separated by commas, semicolon, space, and other)
11. Create interactive HTML using JS

12. Array manipulation
  * Index of specified element
  * Overwrite an element
  * Add an element to start or end
  * Remove selected elements
  * Array concatenation
  * Sort and reverse
13. Creating sub-objects

14. Creating object literals (pseudo-array-objects)

[1]: https://github.com/Az-21/learning-javascript/blob/master/reference/js-ref.pdf
